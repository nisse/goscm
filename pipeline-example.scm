;; Example based on https://github.com/adonovan/gopl.io/blob/master/ch8/pipeline1/main.go

; Run with guile -e main -l go.scm -s pipeline-example.scm
(use-modules (ice-9 format))

(define (main args)
  (let ((naturals (new-channel))
	(squares (new-channel)))

    (go
     (let loop ((x 0))
       (when (< x 50)
	 (channel-send naturals x)
	 (loop (1+ x))))
     (channel-close naturals))

    (go
     (for-channel naturals (x (channel-send squares (* x x))))
     (channel-close squares))

    (for-channel squares (x (format #t "~d\n" x)))))
