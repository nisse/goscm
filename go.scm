;; Implementation of golang-style goroutines, on top of call/cc,
;; single-threaded.
(use-modules (srfi srfi-9))

(define-syntax define-push
  (syntax-rules ()
    ((_ push (getter setter))
     (define-syntax push
       (syntax-rules ()
	 ((_ o x) (setter o (cons x (getter o)))))))))

(define-syntax define-pop
  (syntax-rules ()
    ((_ pop (getter setter))
     (define-syntax pop
       (syntax-rules ()
	 ((_ o) (let ((list (getter o)))
		  (cond ((null? list) #f)
			(else
			 (setter o (cdr list))
			 (car list))))))))))

(define-syntax push!
  (syntax-rules ()
    ((_ place value)
     (set! place (cons value place)))))

;; Returns #f for empty list.
(define-syntax pop!
  (syntax-rules ()
    ((_ place)
     (if (null? place) #f
	 (let ((x (car place)))
	   (set! place (cdr place))
	   x)))))

;; Goroutines ready to be continued.
(define *pending* '())

;; Total number of goroutines blocked on some channel.
(define *blocked-count* 0)
;; Sorted lists of (time . thunk), time is in seconds since unix epoch.
(define *timers* '())

(define (blocked?) (not (zero? *blocked-count*)))
(define (blocked+) (set! *blocked-count* (1+ *blocked-count*)))
(define (blocked-)
  (set! *blocked-count* (1- *blocked-count*))
  (if (< *blocked-count* 0) (error "invalid blocked count" *blocked-count*)))

;; Calls a pending goroutine, never returns.
(define (yield)
  (cond
   ((pop! *pending*)
    => (lambda (next)
	 (next)))
   ((pop! *timers*)
    => (lambda (next)
	 (let ((now (current-time)))
	   (if (< now (car next))
	       (sleep (- (car next) now)))
	   ((cdr next)))))
   (else
     (if (blocked?) (error "deadlock" *blocked-count*)
	 (exit))))
  (yield))

;; TODO: Naming?
(define (sleep-yield delay)
  (when (> delay 0)
    (call/cc (lambda (cc)
	       (push! *timers* (cons (+ (current-time) delay) cc))
	       (yield)))))

(define (go/thunk thunk)
  (call/cc (lambda (cc)
	     (push! *pending* cc)
	     (thunk)
	     (yield))))

(define-syntax go
  (syntax-rules ()
    ((_ exp exp* ...)
     (go/thunk (lambda () exp exp* ...)))))

(define-record-type <channel>
  (make-channel is-closed senders receivers)
  channel?
  (is-closed channel-closed? channel-set-closed)
  ;; For each sender, value (object . sender-continuation)
  (senders channel-senders channel-set-senders!)
  ;; For each receiver, value is the continuation, which expects a
  ;; cons argument. FIXME: Switch to multiple values?
  (receivers channel-receivers channel-set-receivers!))

(define (new-channel) 
  (make-channel #f '() '()))

(define-push channel-push-sender! (channel-senders channel-set-senders!))
(define-pop channel-pop-sender! (channel-senders channel-set-senders!))
(define-push channel-push-receiver! (channel-receivers channel-set-receivers!))
(define-pop channel-pop-receiver! (channel-receivers channel-set-receivers!))

(define (channel-block-sender! channel object) 
    (call/cc (lambda (cc)
	       (channel-push-sender! channel (cons object cc))
	       (blocked+)
	       (yield))))

;; Called on a closed channel. If there are no blocked senders,
;; notifies all receivers that the channel is closed.
(define (channel-notify-closed! channel)
  (when  (null? (channel-senders channel))
    (for-each (lambda (c)
		(set! *pending* (cons (lambda () (c '(#f . #f))) *pending*)))
	      (channel-receivers channel))
    (channel-set-receivers! channel '())))

;; Pops a sender, adds continuation to pending list, and returns
;; (object . #t) on success, #f if there's no sender.
(define (channel-unblock-sender! channel)
  (let ((next (channel-pop-sender! channel)))
    (and next (begin
		(blocked-)
		(push! *pending* (cdr next))
		(if (channel-closed? channel) (channel-notify-closed! channel))
		(list (car next))))))

(define (channel-block-receiver! channel)
  (call/cc (lambda (cc)
	     (channel-push-receiver! channel cc)
	     (blocked+)
	     (yield))))

(define (channel-unblock-receiver! channel object)
  (let ((next (channel-pop-receiver! channel)))
    (and next (begin
		(blocked-)
		(go
		 (next (cons object #t)))
		#t))))

;; Returns true if object could be sent without blocking.
(define (channel-send-nonblock channel object)
  (if (channel-closed? channel) (error "send on closed channel"))
  (channel-unblock-receiver! channel object))
  
(define (channel-send channel object)
  (or (channel-send-nonblock channel object)
      (channel-block-sender! channel object)))

;; Returns a cons, either (object . #t), (#f . 'block) if receive would
;; block, or (#f . #f) if channel is closed.
(define (channel-receive-nonblock channel)
  (or (channel-unblock-sender! channel)
      (cons #f (if (channel-closed? channel) #f 'block))))

;; Returns a cons, either (object . #t) or (#f . #f) if channel is closed.
(define (channel-receive? channel)
  (let ((value (channel-receive-nonblock channel)))
    (if (eq? 'block (cdr value))
	(channel-block-receiver! channel)
	value)))

(define (channel-receive channel)
  (car (channel-receive? channel)))

(define (channel-close channel)
  (if (channel-closed? channel)
      (error "close on closed channel"))
  (channel-set-closed channel #t)
  (channel-notify-closed! channel))

(define (for-channel-worker channel item-proc)
  (let loop ()
    (let ((o (channel-receive? channel)))
      (when (cdr o)
	(item-proc (car o))
	(loop)))))

(define-syntax for-channel
  (syntax-rules ()
    ((_ channel (var exp exp* ...))
     (for-channel-worker channel
			 (lambda (var) exp exp* ...)))))

;; TODO: 
;; * select
